# No 1
Mampu mendemonstrasikan perancangan basis data berdasarkan permasalahan dunia nyata melalui reverse engineering produk digital global. Lampirkan bukti tabel hasil desain

Link dari tabel https://gitlab.com/abdullahazzam2199/praktikum-basis-data/-/blob/main/Jawaban%20Prak%20Basdat.md/DDL_revisi.png

# No 2
Mampu mendemonstrasikan DATA DEFINITION LANGUAGE (DDL) secara tepat berdasarkan minimal 10 entitas dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record.
```sql

CREATE TABLE `Zenius_Dashboard_Murid` (
  `id_akun` integer PRIMARY KEY,
  `Nama` varchar(255),
  `Capaian_Nilai_Murid` varchar(255),
  `Capaian_Materi_Murid` Varchar(255)
);

CREATE TABLE `Data_Akun` (
  `id_akun` integer PRIMARY KEY,
  `password_akun` varchar(255),
  `jangka_waktu_akun` integer,
  `Status_Akun` varchar(255)
);

CREATE TABLE `Zenius_Murid` (
  `id_No_Pendaftaran` integer PRIMARY KEY,
  `Nama` varchar(255),
  `Alamat` varchar(255),
  `Kualitas_Akun` Varchar(255),
  `Profil` Varchar(255)
);

CREATE TABLE `Zenius_Mentor` (
  `Id_Mentor` integer PRIMARY KEY,
  `spesialisasi` varchar(255),
  `Nama` varchar(255),
  `Alamat` varchar(255),
  `Biodata` varchar(255)
);

CREATE TABLE `Komentar` (
  `id_komentar` integer PRIMARY KEY,
  `Tgl_waktu_Komentar` integer,
  `isi_Komentar` varchar(255),
  `platform_komentar` varchar(255)
);

CREATE TABLE `DataBase_Chat` (
  `id_chat` integer PRIMARY KEY,
  `orang_pertama` varchar(255),
  `orang_kedua` varchar(255),
  `isi_chat` varchar(255)
);

CREATE TABLE `Zenius_Admin` (
  `Id_Admin` integer PRIMARY KEY,
  `Nama` varchar(255),
  `Alamat` varchar(255),
  `Biodata` varchar(255)
);

CREATE TABLE `Siaran_Langsung` (
  `id_Siaran_Langsung` integer PRIMARY KEY,
  `Tgl_waktu` integer,
  `Isi` varchar(255)
);

CREATE TABLE `Materi` (
  `id_materi` integer PRIMARY KEY,
  `mata_pelajaran` varchar(255),
  `nama_materi` varchar(255),
  `tingkatan_kelas_materi` integer,
  `tipe_materi` varchar(255),
  `video` varchar(255)
);

CREATE TABLE `soal` (
  `id_soal` integer PRIMARY KEY,
  `mata_pelajaran` varchar(255),
  `soal` varchar(255),
  `tipe_soal` varchar(255),
  `jawaban` varchar(255),
  `nilai` varchar(255)
);

CREATE TABLE `Nilai_Murid` (
  `id_Nilai` integer PRIMARY KEY,
  `nama_Murid` varchar(50),
  `Mata_Pelajaran` varchar(255),
  `nilai` int
);


ALTER TABLE `Komentar` ADD FOREIGN KEY (`id_komentar`) REFERENCES `Zenius_Admin` (`Id_Admin`);

ALTER TABLE `Komentar` ADD FOREIGN KEY (`id_komentar`) REFERENCES `Zenius_Mentor` (`Id_Mentor`);

ALTER TABLE `Siaran_Langsung` ADD FOREIGN KEY (`id_Siaran_Langsung`) REFERENCES `Zenius_Mentor` (`Id_Mentor`);

ALTER TABLE `Siaran_Langsung` ADD FOREIGN KEY (`id_Siaran_Langsung`) REFERENCES `Zenius_Admin` (`Id_Admin`);

ALTER TABLE `DataBase_Chat` ADD FOREIGN KEY (`id_chat`) REFERENCES `Zenius_Mentor` (`Id_Mentor`);

ALTER TABLE `Zenius_Dashboard_Murid` ADD FOREIGN KEY (`id_akun`) REFERENCES `Zenius_Mentor` (`Id_Mentor`);
```

Link no 2:https://gitlab.com/abdullahazzam2199/praktikum-basis-data/-/blob/main/Jawaban%20Prak%20Basdat.md/Jawaban_no_2.mp4

# No 3
Mampu mendemonstrasikan DATA MANIPULATION LANGUAGE (DML) berdaraskan minimal 5 use case operasional (CRUD) dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record.

# A. Use Case operasional create Materi

```sql
Insert Into Materi
(id_materi,mata_pelajaran,nama_materi,tingkatan_kelas_materi,tipe_materi,video)
VALUES
(1,'Matematika',' Aljabar',10,'UTBK',null),
(2,"Fisika","Gerak Lurus",11,"UTBK",null),
(3," Bahasa Inggris","Grammar",12,"USBN",null),
(4,"Kimia","Reaksi Redoks",12,"UTBK",null),
(5,"Biologi","Genetika",10,"USBN",null),
(6,"Sejarah","Revolusi Prancis",11,"USBN",null),
(7,"Geografi","Struktur Bumi",12,"UTBK",null),
(8,"Ekonomi", "Pasar Persaingan Sempurna",10,"USBN",null),
(9, "Sosiologi","Kelompok Sosial",11,"UTBK",null),
(10,"Seni Budaya","Tari Tradisional",12,"USBN",null),
(11,"Pendidikan Agama","Akhlak Mulia",10,"USBN",null),
(12,"Penjaskes","Lompat Tinggi",11,"USBN",null),
(13,"Kewirausahaan","Kreativitas Produk",12,"USBN",null),
(14," Komputer"," Pemrograman Java",10,"USBN",null),
(15,"Akuntansi","Neraca Lajur",11,"UTBK",null),
(16,"Bahasa Indonesia","Ejaan dan Tata Bahasa",12,"UTBK",null),
(17,"Fisika","Medan Listrik",10,"UTBK",null),
(18,"Matematika","Trigonometri",11,"UTBK",null),
(19,"Kimia","Larutan Penyangga",12,"UTBK",null),
(20," Biologi","Sistem Reproduksi Manusia",10,"UTBK",null),
(21,"Sejarah","Perang Dunia II",11,"USBN",null),
(22," Geografi","Perubahan Iklim",12,"UTBK",null),
(23,"Ekonomi","Sistem Ekonomi",10,"UTBK",null),
(24,"Sosiologi"," Konflik Sosial",11,"UTBK",null),
(25," Seni Budaya"," Seni Rupa",12,"USBN",null),
(26," Pendidikan Agama"," Toleransi Beragama",10,"USBN",null),
(27," Penjaskes"," Renang Gaya Bebas",11,"USBN",null);


```

#  B. Use case Operasional Create soal
```sql
`INSERT INTO soal
(id_soal,mata_pelajaran,soal,tipe_soal,jawaban,nilai)
VALUES
(1,"Matematika"," Berapakah hasil dari 5 + 3?","UTBK","8","10"),
(2,"Bahasa Inggris","Translate kalimat I love you ke dalam bahasa Indonesia.","UTBK","Saya mencintaimu","10"),
(3,"Fisika","Berapakah kecepatan suara di udara?","UTBK","343 meter per detik","10"),
(4,"Kimia","Apa simbol kimia untuk unsur hidrogen?","UTBK","H","10"),
(5,"Biologi","Apa fungsi utama dari hati dalam tubuh manusia?","UTBK","Membantu dalam pemrosesan nutrisi dan detoksifikasi","10"),
(6," Sejarah","Siapakah presiden pertama Indonesia?","UTBK","Soekarno","10"),
(7, "Ekonomi","Apa yang dimaksud dengan inflasi?","USBN","Kenaikan umum dan terus-menerus dalam harga barang dan jasa","10"),
(8,"Seni Budaya","Sebutkan tiga jenis seni rupa!","USBN","Lukisan, patung, dan grafik","10"),
(9,"Penjaskes"," Apa manfaat dari olahraga secara teratur?","USBN","Meningkatkan kebugaran fisik dan kesehatan secara keseluruhan","10");
`
```

# C. Use case Operasional Create Zenius_Murid

```sql
`INSERT INTO Zenius_Murid
(id_no_pendaftaran,nama,alamat,kualitas_akun,profil)
VALUES
(1,"John Doe","Jl. ABC No. 123","Premium","Kelas 10 SMA"),
(2,"Jane Smith","Jl. XYZ No. 456","Premium","Kelas 11 SMA"),
(3,"David Johnson","Jl. PQR No. 789","Premium","Kelas 12 SMA"),
(4,"Sarah Williams","Jl. MNO No. 321","Premium","Kelas 10 SMA"),
(5,"Michael Brown"," Jl. DEF No. 654","Premium","Kelas 11 SMA"),
(6,"Emily Johnson","Jl. GHI No. 987","Standar"," Kelas 10 SMA"),
(7,"William Davis","Jl. UVW No. 654","Standar","Kelas 12 SMA"),
(8,"Olivia Wilson"," Jl. JKL No. 321","Standar","Kelas 11 SMA"),
(9,"James Thompson","Jl. RST No. 789","Standar","Kelas 10 SMA"),
(10,"Sophia Anderson","Jl. EFG No. 654","Standar","Kelas 12 SMA");
`
```

# D.Use case Operasional Update Zenius_Murid

```sql
`UPDATE Zenius_Murid SET kualitas_akun=
CASE 
    WHEN nama="John Doe"THEN "Premium"
    WHEN nama="William Davis"THEN "Standar"
    
END WHERE id_No_Pendaftaran IN (1,7); `
```
# E. use case Operasional Update soal
```sql
`UPDATE soal SET tipe_soal=
CASE 
    WHEN id_soal=1 THEN "USBN"
    WHEN id_soal=2 THEN "USBN"
    
END WHERE id_soal IN (1,2); `
```
# F.Use case Operasional Delete Materi
```sql

`DELETE FROM Materi where id_materi=5;
DELETE FROM Materi where id_materi=6;
`

```

# G.Use case Operasional create nilai murid
```sql
INSERT INTO `Nilai_Murid` (`id_Nilai`, `nama_Murid`, `Mata_Pelajaran`, `nilai`) VALUES
(1, 'John Doe', 'Matematika', 90),
(2, 'Jane Smith', 'Bahasa Inggris', 85),
(3, 'Michael Johnson', 'Fisika', 78),
(4, 'Emily Brown', 'Kimia', 92),
(5, 'David Lee', 'Biologi', 88),
(6, 'Olivia Taylor', 'Sejarah', 75),
(7, 'Sophia Anderson', 'Ekonomi', 83),
(8, 'Daniel Martinez', 'Geografi', 87),
(9, 'Isabella Lopez', 'Seni Budaya', 92),
(10, 'Alexander Wilson', 'Bahasa Indonesia', 80),
(11, 'Emma Garcia', 'Komputer', 95),
(12, 'Matthew Rodriguez', 'Olahraga', 88),
(13, 'Ava Hernandez', 'Sosiologi', 82),
(14, 'Benjamin Martinez', 'Agama', 90),
(15, 'Mia Adams', 'Bahasa Asing', 78);
```

Link Video No. 3=
- https://gitlab.com/abdullahazzam2199/praktikum-basis-data/-/blob/main/Jawaban%20Prak%20Basdat.md/video_no_3_part_1_.mp4
- https://gitlab.com/abdullahazzam2199/praktikum-basis-data/-/blob/main/Jawaban%20Prak%20Basdat.md/Video_no_3_part_2_.mp4


# No 4
Mampu mendemonstrasikan DATA QUERY LANGUAGE (DQL) berdaraskan minimal 5 pertanyaan analisis dari produk digital yang dipilih, menggunakan setidaknya keyword GROUP BY, INNER JOIN, LEFT / RIGHT JOIN, AVERAGE / MAX / MIN. Lampirkan bukti berupa source code dan screen record.

# A. Use case operasional Mengurutkan nilai murid dari yang terbesar ke yang terkecil 
```sql
SELECT nama_murid, MAX(nilai) FROM Nilai_Murid GROUP BY nilai DESC;
```

# B. Use case operasional Mengelompokan materi berdasarkan utbk atau usbn 
```sql
SELECT tipe_materi, COUNT(*) FROM Materi GROUP BY tipe_materi;
```

# C. Use case operasional Mengelompokan murid berdasarkan kualitas akun 
```sql
SELECT kualitas_akun, COUNT(*) FROM Zenius_Murid GROUP BY kualitas_akun;
```

# D. Use case operasional mengurutkan seluruh soal berdasarkan tipe soal
```sql
SELECT * FROM soal ORDER BY tipe_soal;
```

# E. Use case operasional mengurutkan seluruh materi berdasarkan tingkatan kelas dari yang paling bawah
```sql
SELECT * FROM Materi ORDER BY tingkatan_kelas_materi ASc;
```

link video=https://gitlab.com/abdullahazzam2199/praktikum-basis-data/-/blob/main/Jawaban%20Prak%20Basdat.md/Video_no_4_DQL.mp4

# No 5

Link Video yt:
https://www.youtube.com/playlist?list=PLmYEBfLi-sVLcms9VFKFaMJjoZufTSAZR
